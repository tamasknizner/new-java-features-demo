package com.demo.main.multithreading;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.CompletableFuture;

public class ThreadsDemo {

    /*
    Jave 8 Async:
        - Future
        - CompletableFuture
     */
    public static void main(String[] args) {
        new ThreadsDemo().run();
    }

    private void run() {
        MyRunnable fiveSec = new MyRunnable("five sec", 5000);
        MyRunnable tenSec = new MyRunnable("ten sec", 10000);

        printTime();

        fiveSec.run();
        tenSec.run();
        /**
         * Runnable-s should be committed to a thread to be asyncronous
         */
//        new Thread(fiveSec).start();
//        new Thread(tenSec).start();

        //printTime();

        /**
         * Java 8 fun
         */
        CompletableFuture<String> s = CompletableFuture.supplyAsync(() -> "asdf");
        CompletableFuture<String> d = CompletableFuture.supplyAsync(() -> "asdf");
        CompletableFuture.allOf(s, d).join();

    }

    class MyRunnable implements Runnable {

        private final String message;
        private final long delay;

        public MyRunnable(String message, long delay) {
            this.message = message;
            this.delay = delay;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(Thread.currentThread().getName() + " " + message);
            printTime();
        }
    }

    private void printTime() {
        System.out.println(LocalDateTime.ofInstant(Instant.ofEpochMilli(System.currentTimeMillis()), ZoneId.systemDefault()));
    }

}
