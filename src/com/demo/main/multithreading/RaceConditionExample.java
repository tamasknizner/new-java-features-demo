package com.demo.main.multithreading;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class RaceConditionExample {

    public static void main(String[] args) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(10);

        Counter counter = new Counter();

        for(int i = 0; i < 1000; i++) {
            executorService.submit(() -> counter.increment());
        }

        executorService.shutdown();
        executorService.awaitTermination(60, TimeUnit.SECONDS);

        System.out.println("Final count is : " + counter.getCount());
    }

    static class Counter {
        private static final Object LOCK = new Object();

        private int count;

        static synchronized void f() {

        }

        /**
         * synchronized -> only one thread can access the block at the same time
         */
        public void increment() {
            // lock
            synchronized (this) {
                count++;
            }
        }

        public int getCount() {
            return count;
        }
    }

//    static class Counter {
//        private AtomicInteger count = new AtomicInteger(0);
//
//        public void increment() {
//            count.incrementAndGet();
//        }
//
//        public int getCount() {
//            return count.intValue();
//        }
//    }
}
