package com.demo.main.sixteen;

public class SealedDemo {

    /**
     * Sealed classes can define which other classes can extend them.
     */
    abstract sealed class Shape permits Rectangle/*, Triangle*/ {

    }

    /**
     * Must be final.
     */
    final class Rectangle extends Shape {}

    /**
     * won't work, because not final
     */
    //class Triangle extends Shape {}

    /**
     * not permitted
     */
    //class Beer extends Shape {}

}
