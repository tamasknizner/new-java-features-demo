package com.demo.main.sixteen;

import java.util.Objects;

public class RecordsDemo {

    public static void main(String[] args) {
        new RecordsDemo().run();
    }

    private void run() {
        Person marty = new Person(1985, "Marty");
        Person otherMarty = new Person(1985, "Marty");
        System.out.println(marty);
        System.out.println(marty.equals(otherMarty));

        BetterPerson joe = new BetterPerson(1973, "Joe Mama");
        BetterPerson otherJoe = new BetterPerson(1973, "Joe Mama");

        System.out.println(joe);
        System.out.println(joe.equals(otherJoe));

        Box<Integer> iBox = new Box<>(10);
        Integer stuff = iBox.stuff();

        /**
         * different method naming convention
         */
        marty.getName();
        joe.name();
    }

    class Person {
        private final int birthYear;
        private final String name;

        public Person(int birthYear, String name) {
            this.birthYear = birthYear;
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Person person = (Person) o;
            return birthYear == person.birthYear && Objects.equals(name, person.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(birthYear, name);
        }

        @Override
        public String toString() {
            return "Person{" +
                    "birthYear=" + birthYear +
                    ", name='" + name + '\'' +
                    '}';
        }

        public int getBirthYear() {
            return birthYear;
        }

        public String getName() {
            return name;
        }
    }

    record BetterPerson(int birthYear, String name){}

    record Box<T>(T stuff){ }
}
