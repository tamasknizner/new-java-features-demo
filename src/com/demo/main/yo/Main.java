package com.demo.main.yo;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        new Main().run();
    }

    private void run() throws InterruptedException {
        // Java 11
        // reserved typename ~ local variable type inference
        var greeting = "Hello World!";
        List<String> stringList = new ArrayList<>();
        var list = new ArrayList<String>();
        list.add(greeting);

        // Java 14
        Days day = Days.MONDAY;
        int numLetters = switch (day) {
            case MONDAY, FRIDAY, SUNDAY -> 6;
            case TUESDAY                -> 7;
            default      -> {
                String s = day.toString();
                yield s.length();
            }
        };

        // Java 15
        // Text Blocks
        String html = """
            <html>
                <body>
                    <p>Hello, world</p>
                </body>
                
                asdf
                
            </html>
            """;
        System.out.println(html);

        // Fancy NPE
        Runnable r = new Runnable() {
            @Override
            public void run() {
                String s = null;
                System.out.println(s.length());
            }
        };
        new Thread(r).start();
        //Thread.sleep(2000);


        // Java 16
        Object obj = getStuff();
        if (obj instanceof String s && s.length() > 5) {
            System.out.println("obj is a String with more than 5 characters: " + s.toUpperCase());
        }
    }

    private Object getStuff() {
        return "asdfasdf";
    }

    enum Days {
        MONDAY, FRIDAY, SUNDAY, TUESDAY
    }
}
