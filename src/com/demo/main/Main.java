package com.demo.main;

import java.util.Optional;

public class Main {

    public static void main(String[] args) {
        /**
         * orElse vs orElseGet
         */
        String a = Optional.ofNullable(getString()).orElse(generateDefault());
        System.out.println("orElse: " + a);

        String b = Optional.ofNullable(getString()).orElseGet(() -> generateDefault());
        System.out.println("orElseGet: " + b);
    }

    static String getString() {
        return "asdfasdf";
    }

    static String generateDefault() {
        System.out.println("generating default...");
        return "DEFAULT";
    }
}
